# Petri

A simulation implementing the concepts brought forth in recent
floating point approaches to simulate a system Similar to Conway's
Game of Life. The agents presented here are randomly selected with an
array of non-newtonian forces that are then distributed into a force
to agent grid.

The specified forces the move the "dots" around a "dish" and simulate their interactions.

A certain level of Brownian motion is also implemented, simply to ensure
that the game board is not left completely motionless.

![an example](./exampleImages/Petri%20(2).PNG "an example scene")

To run the code, you must have GoLand > 1.15 and a moderately decent
CPU. I personally use a r5 3600, and find no issue at the specified 
resolutions.

The command to run, when CD'd into this directory, is:

```bash 
go mod download
go run .
```

### Customizations

The main.go file contains a few constants at the top, please play
around with them, though be aware that large buckets decrease performance
and that I have not tested many inputs for those values.

Additionally, the interactions.yaml file under the assets directory
can be altered to produce non-random interactions between agents.
Please know that this is not well tested.