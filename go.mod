module dot-game-go

go 1.15

require github.com/hajimehoshi/ebiten/v2 v2.1.2

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
