package main

import (
	"math"
	"math/rand"
)

type Vector2 [2]float64

func (v Vector2) Distance(b Vector2) float64 {
	return v.Subtract(b).Magnitude()
}

func (v Vector2) Magnitude() float64 {
	return math.Sqrt(v[0]*v[0] + v[1]*v[1])
}

func (v Vector2) Subtract(b Vector2) Vector2 {
	return Vector2{v[0] - b[0], v[1] - b[1]}
}

func (v Vector2) Add(b Vector2) Vector2 {
	return Vector2{v[0] + b[0], v[1] + b[1]}
}

func (v Vector2) Scale(s float64) Vector2 {
	return Vector2{v[0] * s, v[1] * s}
}

func (v Vector2) Bucket(i uint) (int, int) {
	return int(v[0]) >> i, int(v[1]) >> i
}

func InUnitCircle() Vector2 {
	r := math.Sqrt(rand.Float64())
	t := rand.Float64() * 2 * 3.14159265
	return Vector2{r * math.Cos(t), r * math.Sin(t)}
}
