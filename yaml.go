package main

import (
	"errors"
	"fmt"
	"github.com/go-yaml/yaml"
	"image/color"
	"io/ioutil"
	"os"
)

type dataFormat struct {
	Colors       []string    `yaml:"colors,flow"`
	Interactions [][]float64 `yaml:"interactions,flow"`
}

func init() {
	bytes, _ := yaml.Marshal(&dataFormat{
		Colors: []string{
			"FF557711",
		},
		Interactions: [][]float64{
			{
				0, 1, 2,
			}, {
				0.5, 1.5, 2.5,
			},
		},
	})
	fmt.Printf("[%s]\n", bytes)
}

func getYamlImport(filepath string) error {
	file, err := os.OpenFile(filepath, os.O_RDONLY, 0666)
	if err != nil {
		return err
	}
	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}
	df := &dataFormat{}
	err = yaml.Unmarshal(bytes, df)
	if err != nil {
		return err
	}
	for i := class(0); i < ball_count_max && i < class(len(df.Colors)); i++ {
		if len(df.Colors[i]) == 0 {
			continue
		}
		c, err := ParseHexColorFast(df.Colors[i])
		if err != nil {
			return err
		}
		BallColors[i] = c
	}

	for i := class(0); i < ball_count_max && i < class(len(df.Interactions)); i++ {
		for j := class(0); j < ball_count_max && j < class(len(df.Interactions[i])); j++ {
			interactionMatrix[i][j] = df.Interactions[i][j]
		}
	}

	return nil
}

var errInvalidFormat = errors.New("invalid format")

func ParseHexColorFast(s string) (c color.RGBA, err error) {
	c.A = 0xff

	if s[0] != '#' {
		return c, errInvalidFormat
	}

	hexToByte := func(b byte) byte {
		switch {
		case b >= '0' && b <= '9':
			return b - '0'
		case b >= 'a' && b <= 'f':
			return b - 'a' + 10
		case b >= 'A' && b <= 'F':
			return b - 'A' + 10
		}
		err = errInvalidFormat
		return 0
	}

	switch len(s) {
	case 7:
		c.R = hexToByte(s[1])<<4 + hexToByte(s[2])
		c.G = hexToByte(s[3])<<4 + hexToByte(s[4])
		c.B = hexToByte(s[5])<<4 + hexToByte(s[6])
	case 4:
		c.R = hexToByte(s[1]) * 17
		c.G = hexToByte(s[2]) * 17
		c.B = hexToByte(s[3]) * 17
	default:
		err = errInvalidFormat
	}
	return
}
