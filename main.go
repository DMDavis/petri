package main

import (
	"fmt"
	"github.com/hajimehoshi/ebiten/v2"
	ebitenutil "github.com/hajimehoshi/ebiten/v2/inpututil"
	"log"
	"math/rand"
	"sync"
	"time"
)

const (
	bucketSize   = 4
	bucketsWide  = 80
	bucketsTall  = 60
	screenWidth  = bucketsWide * (1 << bucketSize)
	screenHeight = bucketsTall * (1 << bucketSize)
)

type Game struct {
	Balls    []ball
	simulate bool
}

func (g *Game) Update() error {

	if ebitenutil.IsKeyJustPressed(ebiten.KeySpace) {
		g.simulate = !g.simulate
	}

	if !g.simulate {
		return nil
	}
	var buckets [bucketsWide + 2][bucketsTall + 2]bucket
	for i := range g.Balls {
		x, y := g.Balls[i].Position.Bucket(bucketSize)
		x = ((x % bucketsWide) + bucketsWide) % bucketsWide
		y = ((y % bucketsTall) + bucketsTall) % bucketsTall
		if buckets[x+1][y+1] == nil {
			buckets[x+1][y+1] = &directBucket{}
		}
		buckets[x+1][y+1].AddBall(&g.Balls[i])
	}

	for i := 0; i < len(buckets); i++ {
		for j := 0; j < len(buckets[i]); j++ {
			if i == 0 || j == 0 || i == len(buckets)-1 || j == len(buckets[i])-1 {
				x := (((i - 1%bucketsWide) + bucketsWide) % bucketsWide) + 1
				y := (((j - 1%bucketsTall) + bucketsTall) % bucketsTall) + 1
				buckets[i][j] = newCopyBucket(buckets[x][y], Vector2{float64(i - x), float64(j - y)}.Scale(1<<bucketSize))
			}
		}
	}

	check := sync.WaitGroup{}

	for x := 1; x < len(buckets)-1; x++ {
		col := buckets[x]
		neighborColumns := [][]bucket{
			buckets[x-1][:],
			buckets[x][:],
			buckets[x+1][:],
		}
		for y := 1; y < len(col)-1; y++ {
			row := buckets[x][y]
			neighborCells := []bucket{
				neighborColumns[0][y-1],
				neighborColumns[1][y-1],
				neighborColumns[2][y-1],
				neighborColumns[0][y],
				neighborColumns[1][y],
				neighborColumns[2][y],
				neighborColumns[0][y+1],
				neighborColumns[1][y+1],
				neighborColumns[2][y+1],
			}
			if row == nil {
				continue
			}
			cell := row.GetBalls()
			for _, b := range cell {
				check.Add(1)
				go func(ball *ball, group *sync.WaitGroup) {
					ball.computeForces(neighborCells)
					group.Done()
				}(b, &check)
			}
		}
	}

	check.Wait()

	for i := range g.Balls {
		g.Balls[i].movement(time.Second / 60)
	}
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	for _, ball := range g.Balls {
		ball.Draw(screen)
	}
}

func (g *Game) Layout(_, _ int) (int, int) {
	return screenWidth, screenHeight
}

func main() {
	err := getYamlImport("./assets/interactions.yaml")
	if err != nil {
		panic(err)
	}
	center := Vector2{screenWidth / 2, screenHeight / 2}

	balls := make([]ball, 8192)
	for i := range balls {
		balls[i] = ball{
			Position: center.Add(InUnitCircle().Scale(500)),
			Velocity: Vector2{0.0, 0.0},
			Class:    class(rand.Int63n(int64(ball_count_max))),
		}
	}

	fmt.Println(len(balls))

	g := &Game{
		Balls: balls,
	}

	ebiten.SetMaxTPS(ebiten.DefaultTPS * 2)

	ebiten.SetWindowSize(screenWidth, screenHeight)
	ebiten.SetWindowTitle("Petri")
	if err := ebiten.RunGame(g); err != nil {
		log.Fatal(err)
	}
}
