package main

type bucket interface {
	GetBalls() []*ball
	AddBall(*ball)
}

type directBucket struct {
	balls []*ball
}

func (d *directBucket) GetBalls() []*ball {
	return d.balls
}

func (d *directBucket) AddBall(b *ball) {
	d.balls = append(d.balls, b)
}

func newCopyBucket(ori bucket, offset Vector2) *copyBucket {

	var original []*ball
	if ori != nil {
		original = ori.GetBalls()
	}

	c := &copyBucket{
		ballCopies:   make([]ball, len(original)),
		ballPointers: make([]*ball, len(original)),
	}
	for i := range c.ballCopies {
		c.ballCopies[i] = *original[i]
		c.ballCopies[i].Position = c.ballCopies[i].Position.Add(offset)
		c.ballPointers[i] = &c.ballCopies[i]
	}
	return c
}

type copyBucket struct {
	ballCopies   []ball
	ballPointers []*ball
}

func (c *copyBucket) GetBalls() []*ball {
	return c.ballPointers
}

func (c *copyBucket) AddBall(b *ball) {
	c.ballCopies = append(c.ballCopies, *b)
	c.ballPointers = append(c.ballPointers, &c.ballCopies[len(c.ballCopies)-1])
}
