package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"image/color"
	"image/png"
	"math"
	"math/rand"
	"os"
	"time"
)

const FORCE_DISTANCE = 12.0
const FORCE_WIDTH = 12.0
const FRICTION = 0.90

type class uint8

const (
	BallBlue = class(iota)
	BallRed
	BallGreen
	BallOrange
	BallViolet
	BallPink
	BallSky
	ball_count_max
)

var BallColors = [ball_count_max]color.Color{
	BallBlue:   color.RGBA{0x05, 0x07, 0xDE, 0xFF},
	BallRed:    color.RGBA{0xD5, 0x07, 0x02, 0xFF},
	BallGreen:  color.RGBA{0x05, 0xD7, 0x01, 0xFF},
	BallOrange: color.RGBA{0xDF, 0x92, 0x16, 0xFF},
	BallViolet: color.RGBA{0x61, 0x3F, 0x75, 0xFF},
	BallPink:   color.RGBA{0xE8, 0x41, 0x8B, 0xFF},
	BallSky:    color.RGBA{0x9B, 0xAB, 0xFF, 0xFF},
}

var interactionMatrix = [ball_count_max][ball_count_max]float64{
	//           BLU   RED   GRN   ONG   VLT    PNK   SKY
	BallBlue:   {0.4, -0.1, 0.5, -0.6, 0.22, -0.3, 0.4},
	BallRed:    {0.1, 0.2, 0.2, 0.7, 0.70, -0.7, 0.1},
	BallGreen:  {0.4, -0.1, 1.2, -0.7, 0.12, -0.8, 0.4},
	BallOrange: {0.6, -1.0, -1.2, -0.7, -0.22, -0.7, -0.5},
	BallViolet: {0.4, 0.1, -0.2, 0.3, -0.72, -0.7, 0.2},
	BallPink:   {0.4, -0.3, 0.8, -0.6, 0.22, 0.2, -0.3},
	BallSky:    {0.4, -0.3, 0.8, -0.7, 0.22, 0.2, -0.2},
}

const isRandom = true

func init() {
	rand.Seed(time.Now().Unix())
	if isRandom {
		for i := range interactionMatrix {
			for j := range interactionMatrix[i] {
				interactionMatrix[i][j] = rand.NormFloat64()
			}
		}
	}
}

var spriteImage *ebiten.Image

func init() {
	rand.Seed(time.Now().Unix())
	file, err := os.OpenFile("assets/textures/sprite.png", os.O_RDONLY, 0666)
	if err != nil {
		panic(err)
	}
	img, err := png.Decode(file)
	if err != nil {
		panic(err)
	}
	spriteImage = ebiten.NewImageFromImage(img)
}

type ball struct {
	Velocity, Position Vector2
	InverseMass        float64
	Class              class
}

const doBrownian = true

func (b *ball) computeForces(otherBuckets []bucket) {
	for _, bucket := range otherBuckets {
		if bucket == nil {
			continue
		}
		otherBalls := bucket.GetBalls()
		for _, other := range otherBalls {
			diff := other.Position.Subtract(b.Position)

			dist := diff.Magnitude()
			if dist < 0.0001 {
				continue
			}

			force := 0.0
			if dist < FORCE_DISTANCE {
				force = -8 / (dist)
			} else if dist < FORCE_DISTANCE+2*FORCE_WIDTH {
				scalar := interactionMatrix[b.Class][other.Class]
				force = math.Abs(dist-FORCE_DISTANCE-FORCE_WIDTH)*scalar/FORCE_WIDTH + scalar
			}
			unit := diff.Scale(1 / (dist * dist))
			b.Velocity = b.Velocity.Add(unit.Scale(force))
			if b.Velocity.Magnitude() > 1000 {
				b.Class = (b.Class + 1) % ball_count_max
			}
		}
	}
	if doBrownian {
		b.Velocity = b.Velocity.Add(InUnitCircle().Scale(3))
	}
}

func (b *ball) movement(delta time.Duration) {

	b.Velocity = b.Velocity.Scale(FRICTION)

	if b.Position[0] < 0 {
		b.Position[0] += screenWidth
	} else if b.Position[0] > screenWidth {
		b.Position[0] -= screenWidth
	}
	if b.Position[1] < 0 {
		b.Position[1] += screenHeight
	} else if b.Position[1] > screenHeight {
		b.Position[1] -= screenHeight
	}
	if b.Velocity[0] > 60 {
		b.Velocity[0] = 60
	} else if b.Velocity[0] < -60 {
		b.Velocity[0] = -60
	}
	if b.Velocity[1] > 60 {
		b.Velocity[1] = 60
	} else if b.Velocity[1] < -60 {
		b.Velocity[1] = -60
	}

	b.Position = b.Position.Add(b.Velocity.Scale(delta.Seconds()))
}

func (b *ball) Draw(img *ebiten.Image) {
	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(b.Position[0], b.Position[1])
	op.ColorM.Scale(colorToScale(BallColors[b.Class]))
	img.DrawImage(spriteImage, op)
}

func colorToScale(clr color.Color) (float64, float64, float64, float64) {
	r, g, b, a := clr.RGBA()
	rf := float64(r) / 0xffff
	gf := float64(g) / 0xffff
	bf := float64(b) / 0xffff
	af := float64(a) / 0xffff
	// Convert to non-premultiplied alpha components.
	if 0 < af {
		rf /= af
		gf /= af
		bf /= af
	}
	return rf, gf, bf, af
}
